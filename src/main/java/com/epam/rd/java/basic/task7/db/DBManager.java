package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance = null;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		List<User> allUsers = new ArrayList<>();
		String sql = "SELECT * FROM users";
		try (Connection connection = DBConnector.getDBConnection();
			 PreparedStatement statement = connection.prepareStatement(sql)) {
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				User user = User.createUser(resultSet.getString("login"));
				user.setId(resultSet.getInt("id"));
				allUsers.add(user);
			}
		} catch (SQLException e) {
			throw new DBException("findAllUsers failed", e);
		}
		return allUsers;
	}

	public boolean insertUser(User user) throws DBException {
		String sql = "INSERT INTO users (login) VALUES (?)";
		try (Connection connection = DBConnector.getDBConnection();
			 PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1, user.getLogin());
			statement.executeUpdate();
			ResultSet resultSet = statement.getGeneratedKeys();
			if (resultSet.next()) {
				user.setId(resultSet.getInt(1));
			}
		} catch (SQLException e) {
			throw new DBException("insertUser failed", e);
		}
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		StringJoiner joiner = new StringJoiner(", ", "(", ")");
		for (int i = 0; i < users.length; i++) {
			joiner.add("?");
		}
		String sql = "DELETE FROM users WHERE users.id in " + joiner;
		try (Connection connection = DBConnector.getDBConnection();
			 PreparedStatement statement = connection.prepareStatement(sql)) {
			int parameterIndice = 1;
			for (User user : users) {
				statement.setInt(parameterIndice, user.getId());
				parameterIndice++;
			}
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DBException("deleteUser failed", e);
		}
		return true;
	}

	public User getUser(String login) throws DBException {
		String sql = "SELECT * FROM users WHERE login = ?";
		try (Connection connection = DBConnector.getDBConnection();
			 PreparedStatement statement = connection.prepareStatement(sql)) {
			statement.setString(1, login);
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				User user = User.createUser(login);
				user.setId(resultSet.getInt("id"));
				return user;
			} else {
				return null;
			}
		} catch(SQLException e) {
			throw new DBException("getUser failed", e);
		}
	}

	public Team getTeam(String name) throws DBException {
		String sql = "SELECT * FROM teams WHERE teams.name = ?";
		try (Connection connection = DBConnector.getDBConnection();
			 PreparedStatement statement = connection.prepareStatement(sql)) {
			statement.setString(1, name);
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				Team team = Team.createTeam(name);
				team.setId(resultSet.getInt("id"));
				return team;
			} else {
				return null;
			}
		} catch(SQLException e) {
			throw new DBException("getTeam failed", e);
		}
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> allTeams = new ArrayList<>();
		String sql = "SELECT * FROM teams";
		try (Connection connection = DBConnector.getDBConnection();
			 PreparedStatement statement = connection.prepareStatement(sql)) {
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Team team = Team.createTeam(resultSet.getString("name"));
				team.setId(resultSet.getInt("id"));
				allTeams.add(team);
			}
		} catch (SQLException e) {
			throw new DBException("findAllUsers failed", e);
		}
		return allTeams;
	}

	public boolean insertTeam(Team team) throws DBException {
		String sql = "INSERT INTO teams (name) VALUES (?)";
		try (Connection connection = DBConnector.getDBConnection();
			 PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1, team.getName());
			statement.executeUpdate();
			ResultSet resultSet = statement.getGeneratedKeys();
			if (resultSet.next()) {
				team.setId(resultSet.getInt(1));
			}
		} catch (SQLException e) {
			throw new DBException("insertTeam failed", e);
		}
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		String sql = "INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)";
		try (Connection connection = DBConnector.getDBConnection();
			 PreparedStatement statement = connection.prepareStatement(sql)) {
			connection.setAutoCommit(false);
			try {
				for (Team team : teams) {
					statement.setInt(1, user.getId());
					statement.setInt(2, team.getId());
					statement.addBatch();
				}
				statement.executeBatch();
				connection.commit();
			} catch (SQLException e) {
				connection.rollback();
				throw new DBException("transaction failed, rolling back...", e);
			}
		} catch (SQLException e) {
			throw new DBException("setTeamsForUser failed", e);
		}
		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> userTeams = new ArrayList<>();
		String sql = "SELECT teams.name FROM users_teams JOIN teams on users_teams.team_id = teams.id" +
				" WHERE users_teams.user_id = ?";
		try (Connection connection = DBConnector.getDBConnection();
			 PreparedStatement statement = connection.prepareStatement(sql)) {
			statement.setInt(1, user.getId());
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				userTeams.add(Team.createTeam(resultSet.getString("name")));
			}
		} catch (SQLException e) {
			throw new DBException("getUserTeams failed", e);
		}
		return userTeams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		String sql = "DELETE FROM teams WHERE teams.id = ?";
		try (Connection connection = DBConnector.getDBConnection();
			 PreparedStatement statement = connection.prepareStatement(sql)) {
			statement.setInt(1, team.getId());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DBException("deleteTeam failed", e);
		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		String sql = "UPDATE teams SET teams.name = ? WHERE teams.id = ?";
		try (Connection connection = DBConnector.getDBConnection();
			 PreparedStatement statement = connection.prepareStatement(sql)) {
			statement.setString(1, team.getName());
			statement.setInt(2, team.getId());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DBException("updateTeam failed", e);
		}
		return true;
	}

}
